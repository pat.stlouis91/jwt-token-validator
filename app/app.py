from flask import Flask, render_template, request
from tokens import generate_token, token_to_dicts, validate_token_2, validate_token_3
from base64 import b64decode

import json

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')


@app.route("/defi1", methods=['GET', 'POST'])
def defi1():
    if request.method == 'GET':
        return render_template('defi1.html')
    else:
        username = request.form['username']
        token = request.form['token']

        # Si le token n'existe pas, générer un nouveau token
        if not token:
            token = generate_token(username)
        
        header, payload, signature = token_to_dicts(token)
        user = payload['data']['username']
        role = payload['data']['role']
        return render_template('defi1.html', username=user, token=token, role=role)


@app.route("/defi2", methods=['GET', 'POST'])
def defi2():
    if request.method == 'GET':
        return render_template('defi2.html')
    else:
        username = request.form['username']
        token = request.form['token']

        # Si le token n'existe pas, générer un nouveau token
        # Sinon, valider celui fourni
        if not token:
            token = generate_token(username)
        else:
            token_valid = validate_token_2(token)
            if not token_valid:
                return render_template('defi2.html', erreur='Token invalide')
        
        header, payload, signature = token_to_dicts(token)
        user = payload['data']['username']
        role = payload['data']['role']
        return render_template('defi2.html', username=user, token=token, role=role)


@app.route("/defi3", methods=['GET', 'POST'])
def defi3():
    if request.method == 'GET':
        return render_template('defi3.html')
    else:
        username = request.form['username']
        token = request.form['token']

        # Si le token n'existe pas, générer un nouveau token
        # Sinon, valider celui fourni
        if not token:
            token = generate_token(username)
        else:
            token_valid = validate_token_3(token)
            if not token_valid:
                return render_template('defi3.html', erreur='Token invalide')
        
        header, payload, signature = token_to_dicts(token)
        user = payload['data']['username']
        role = payload['data']['role']
        return render_template('defi3.html', username=user, token=token, role=role)