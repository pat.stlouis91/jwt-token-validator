FROM python:3.9.7

COPY ["app", "/app"]

WORKDIR /app

RUN ["python3", "-m", "pip", "install", "-r", "/app/requirements.txt"]

COPY ["jwa.py", "/usr/local/lib/python3.9/site-packages/jwt/jwa.py"]

EXPOSE 5000

CMD ["flask", "run", "--host", "0.0.0.0"]