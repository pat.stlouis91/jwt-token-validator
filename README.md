# Description
Projet pour tester la validité et les vulnérabilités des tokens JWT.

# Prérequis
- docker
- docker-compose

# Installation
```bash
# Télécharger le repo
git clone https://gitlab.com/sebast331-ctf/jwt-token-validator
cd jwt-token-validator

# Lancer l'application sur le port 5000
docker-compose up
```

Par la suite, lancer l'adresse http://localhost:5000.

# Preview
![](image.png)

# Défis
Les défis consistent à envoyer un token JWT modifié et à le faire accepter. Pour que le défi soit considéré comme complété, on doit pouvoir forger un token avec le rôle **admin**.

## Défi 1
La signature n'est pas vérifiée. Simplement retirer la signature et modifier le token.

- Référence : https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens#is-the-token-checked

## Défi 2
La signature est vérifiée selon l'algorithme présent. Changer l'algorithme pour *none*, supprimer la signature et modifier le token.

- Référence : https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens#modify-the-algorithm-to-none-cve-2015-9235

## Défi 3
La signature est vérifiée avec la clé publique. Cependant, l'adresse de la clé est modifiable.
1. Générer une paire de clés (https://mkjwk.org)
1. Enregistrer la paire de clé publique/privée sur le disque dans un dossier nommé "static"
1. Modifier le token (role), changer l'url pour l'adresse de son PC et le signer à l'aide de la nouvelle clé privée (https://jwt.io)
   - On doit utiliser le formet X.509
1. Lancer un serveur web, de sorte que le fichier `http://<ip>:<port>/static/public.key` donne la clé publique
1. Envoyer le token. Celui-ci demandera au serveur en référence la clé et vérifiera la signature.

- Référence : https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens#x5u

# Références
https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens